To run the sample locally from Visual Studio:

1. Build the project.
2. Open the Package Manager Console (Tools > NuGet Package Manager > Package Manager Console)
3. In the Package Manager Console window, enter the following command: `Update-Database`
4. Press F5 to debug.

