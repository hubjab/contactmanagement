﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ContactManagementService.Models;

namespace ContactManagementService.Controllers
{
    public class ContactsController : ApiController
    {
        private ContactManagementServiceContext db = new ContactManagementServiceContext();

        public class SearchCriteria
        {
             public string Name { get; set; }
             public string JobTitle { get; set; }
             public bool? Enabled { get; set; }
        }

        // GET: api/Contacts?Name=SampleName&JobTitle=SampleTitle&Enabled=true
        public IQueryable<Contact> GetContacts([FromUri] SearchCriteria searchCriteria)
        {
            IQueryable<Contact> result = db.Contacts;
            
            var name = searchCriteria.Name;
            if (!String.IsNullOrEmpty(name))
            {
                result = result.Where(x => x.Name.ToLower().Contains(name.ToLower()));
            }
            
            var jobTitle = searchCriteria.JobTitle;
            if (!String.IsNullOrEmpty(jobTitle))
            {
                result = result.Where(x => x.JobTitle.ToLower().Contains(jobTitle.ToLower()));
            }
            
            var enabled = searchCriteria.Enabled;
            if (enabled.HasValue)
            {
                result = result.Where(x => x.Enabled == enabled);
            }
            
            return result;
        }

        // GET: api/Contacts/5
        [ResponseType(typeof(Contact))]
        public async Task<IHttpActionResult> GetContact(int id)
        {
            Contact contact = await db.Contacts.FindAsync(id);
            if (contact == null)
            {
                return NotFound();
            }

            return Ok(contact);
        }

        // PUT: api/Contacts/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutContact(int id, Contact contact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != contact.Id)
            {
                return BadRequest();
            }

            db.Entry(contact).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ContactExists(int id)
        {
            return db.Contacts.Count(e => e.Id == id) > 0;
        }
    }
}