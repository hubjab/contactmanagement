﻿function Contacts() {
    var self = this;

    self.SearchResults = ko.observableArray([]);

    self.SearchResultsCount = ko.computed(function () {
        return self.SearchResults().length;
    });

    self.AnyContactsFound = ko.computed(function () {
        return self.SearchResultsCount() > 0;
    });

    self.ActiveContact = ko.observable();
    self.ActiveTemplateName = ko.observable("");

    self.NameSearchText = ko.observable("").extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 500 } });
    self.JobTitleSearchText = ko.observable('').extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 500 } });

    self.ContactStates = ['Any', 'Enabled', 'Disabled'];
    self.SelectedSearchStateName = ko.observable();
    self.SelectedSearchState = ko.computed(function () {
        if (self.SelectedSearchStateName() == "Enabled")
            return true;
        if (self.SelectedSearchStateName() == "Disabled")
            return false;
        return null;
    }, self);

    self.ShowDetailsView = function () {
        self.ActiveTemplateName("details");
    }
    self.ShowSearchView = function () {
        self.ActiveTemplateName("search");
    }

    self.ShowContactDetails = function (contact) {
        self.ActiveContact(contact);
        self.ActiveContact().ResetState();
        self.ShowDetailsView();
    };

    self.SearchInProgress = ko.observable(false);

    self.ShouldDisplayResultsTable = ko.computed(function () {
        return !self.SearchInProgress() && self.AnyContactsFound();
    });

    self.ErrorMessageVisible = ko.observable(false);
    self.CloseAlert = function () {
        self.ErrorMessageVisible(false);
    }
    self.ShowAlert = function () {
        self.ErrorMessageVisible(true);
    }

    self.LastRequest = null;
    self.Search = function () {
        var searchCriteria = {
            Name: self.NameSearchText(),
            JobTitle: self.JobTitleSearchText(),
            Enabled: self.SelectedSearchState()
        }

        if (self.LastRequest != null && self.SearchInProgress()) {
            self.SearchInProgress(false);
            self.LastRequest.abort();
        }

        self.SearchInProgress(true);
        self.LastRequest = $.ajax({
            url: '/api/Contacts',
            type: 'GET',
            data: jQuery.param(searchCriteria),
            success: function (data) {
                self.CloseAlert();
                self.SearchResults.removeAll();
                $.each(data, function (index, item) {
                    self.SearchResults.push(new Contact({
                        Id: item.Id,
                        Name: item.Name,
                        JobTitle: item.JobTitle,
                        PhoneNumber: item.PhoneNumber,
                        Enabled: item.Enabled
                    }));
                });
            },
            error: function () {
                if (self.SearchInProgress())
                    self.ShowAlert();
            },
            complete: function () {
                self.SearchInProgress(false);
            }
        });
    };

    self.NameSearchText.subscribe(function () { self.Search(); }, this);
    self.JobTitleSearchText.subscribe(function () { self.Search(); }, this);
    self.SelectedSearchState.subscribe(function () { self.Search(); }, this);

    self.ShowSearchView();
    self.Search();
}