﻿function Contact(contactData) {
    var self = this;
    self.Id = ko.observable(contactData.Id);
    self.Name = ko.observable(contactData.Name);
    self.JobTitle = ko.observable(contactData.JobTitle);
    self.PhoneNumber = ko.observable(contactData.PhoneNumber);
    self.Enabled = ko.observable(contactData.Enabled);

    self.IsInEditMode = ko.observable(false);

    self.NameDraft = ko.observable();
    self.JobTitleDraft = ko.observable();
    self.PhoneNumberDraft = ko.observable();
    self.EnabledDraft = ko.observable();

    self.ResetDrafts = function() {
        self.NameDraft(self.Name());
        self.JobTitleDraft(self.JobTitle());
        self.PhoneNumberDraft(self.PhoneNumber());
        self.EnabledDraft(self.Enabled());
    }

    self.AcceptDrafts = function() {
        self.Name(self.NameDraft());
        self.JobTitle(self.JobTitleDraft());
        self.PhoneNumber(self.PhoneNumberDraft());
        self.Enabled(self.EnabledDraft());
    }

    self.EnableEditMode = function () {
        self.IsInEditMode(true);
        self.CloseAlert();
        self.CloseSuccessMessage();
    }

    self.DisableEditMode = function () {
        self.IsInEditMode(false);
    }

    self.ErrorMessageVisible = ko.observable(false);
    self.CloseAlert = function () {
        self.ErrorMessageVisible(false);
    }
    self.ShowAlert = function () {
        self.ErrorMessageVisible(true);
    }

    self.SuccessMessageVisible = ko.observable(false);
    self.CloseSuccessMessage = function () {
        self.SuccessMessageVisible(false);
    }
    self.ShowSuccessMessage = function () {
        self.SuccessMessageVisible(true);
    }

    self.SaveInProgress = ko.observable(false);

    self.SaveChanges = function () {
        var updatedContact = {
            Id: self.Id(),
            Name: self.Name(),
            JobTitle: self.JobTitle(),
            PhoneNumber: self.PhoneNumber(),
            Enabled: self.Enabled()
        }
        self.SaveInProgress(true);

        $.ajax({
            url: '/api/Contacts/' + self.Id(),
            type: 'PUT',
            contentType: "application/json",
            data: JSON.stringify(updatedContact),
            success: function () {
                self.AcceptDrafts();
                self.DisableEditMode();
                self.ShowSuccessMessage();
            },
            error: function () {
                self.ShowAlert();
            },
            complete: function() {
                self.SaveInProgress(false);
            }
        });
    }

    self.ResetState = function() {
        self.ResetDrafts();
        self.DisableEditMode();
        self.CloseSuccessMessage();
        self.CloseAlert();
    }

    self.ResetState();
}