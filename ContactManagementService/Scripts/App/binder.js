﻿LoadTemplatesAndBind = function () {
    $.get("../Views/Templates/bundledTemplates.min.html", function (templates) {
        $("body").append(templates);
        var contactsViewModel = new Contacts();
        ko.applyBindings(contactsViewModel);
    });
};
LoadTemplatesAndBind();
