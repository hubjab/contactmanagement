﻿///#source 1 1 /Scripts/App/Contact.js
function Contact(contactData) {
    var self = this;
    self.Id = ko.observable(contactData.Id);
    self.Name = ko.observable(contactData.Name);
    self.JobTitle = ko.observable(contactData.JobTitle);
    self.PhoneNumber = ko.observable(contactData.PhoneNumber);
    self.Enabled = ko.observable(contactData.Enabled);

    self.IsInEditMode = ko.observable(false);

    self.NameDraft = ko.observable();
    self.JobTitleDraft = ko.observable();
    self.PhoneNumberDraft = ko.observable();
    self.EnabledDraft = ko.observable();

    self.ResetDrafts = function() {
        self.NameDraft(self.Name());
        self.JobTitleDraft(self.JobTitle());
        self.PhoneNumberDraft(self.PhoneNumber());
        self.EnabledDraft(self.Enabled());
    }

    self.AcceptDrafts = function() {
        self.Name(self.NameDraft());
        self.JobTitle(self.JobTitleDraft());
        self.PhoneNumber(self.PhoneNumberDraft());
        self.Enabled(self.EnabledDraft());
    }

    self.EnableEditMode = function () {
        self.IsInEditMode(true);
        self.CloseAlert();
        self.CloseSuccessMessage();
    }

    self.DisableEditMode = function () {
        self.IsInEditMode(false);
    }

    self.ErrorMessageVisible = ko.observable(false);
    self.CloseAlert = function () {
        self.ErrorMessageVisible(false);
    }
    self.ShowAlert = function () {
        self.ErrorMessageVisible(true);
    }

    self.SuccessMessageVisible = ko.observable(false);
    self.CloseSuccessMessage = function () {
        self.SuccessMessageVisible(false);
    }
    self.ShowSuccessMessage = function () {
        self.SuccessMessageVisible(true);
    }

    self.SaveInProgress = ko.observable(false);

    self.SaveChanges = function () {
        var updatedContact = {
            Id: self.Id(),
            Name: self.Name(),
            JobTitle: self.JobTitle(),
            PhoneNumber: self.PhoneNumber(),
            Enabled: self.Enabled()
        }
        self.SaveInProgress(true);

        $.ajax({
            url: '/api/Contacts/' + self.Id(),
            type: 'PUT',
            contentType: "application/json",
            data: JSON.stringify(updatedContact),
            success: function () {
                self.AcceptDrafts();
                self.DisableEditMode();
                self.ShowSuccessMessage();
            },
            error: function () {
                self.ShowAlert();
            },
            complete: function() {
                self.SaveInProgress(false);
            }
        });
    }

    self.ResetState = function() {
        self.ResetDrafts();
        self.DisableEditMode();
        self.CloseSuccessMessage();
        self.CloseAlert();
    }

    self.ResetState();
}
///#source 1 1 /Scripts/App/Contacts.js
function Contacts() {
    var self = this;

    self.SearchResults = ko.observableArray([]);

    self.SearchResultsCount = ko.computed(function () {
        return self.SearchResults().length;
    });

    self.AnyContactsFound = ko.computed(function () {
        return self.SearchResultsCount() > 0;
    });

    self.ActiveContact = ko.observable();
    self.ActiveTemplateName = ko.observable("");

    self.NameSearchText = ko.observable("").extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 500 } });
    self.JobTitleSearchText = ko.observable('').extend({ rateLimit: { method: "notifyWhenChangesStop", timeout: 500 } });

    self.ContactStates = ['Any', 'Enabled', 'Disabled'];
    self.SelectedSearchStateName = ko.observable();
    self.SelectedSearchState = ko.computed(function () {
        if (self.SelectedSearchStateName() == "Enabled")
            return true;
        if (self.SelectedSearchStateName() == "Disabled")
            return false;
        return null;
    }, self);

    self.ShowDetailsView = function () {
        self.ActiveTemplateName("details");
    }
    self.ShowSearchView = function () {
        self.ActiveTemplateName("search");
    }

    self.ShowContactDetails = function (contact) {
        self.ActiveContact(contact);
        self.ActiveContact().ResetState();
        self.ShowDetailsView();
    };

    self.SearchInProgress = ko.observable(false);

    self.ShouldDisplayResultsTable = ko.computed(function () {
        return !self.SearchInProgress() && self.AnyContactsFound();
    });

    self.ErrorMessageVisible = ko.observable(false);
    self.CloseAlert = function () {
        self.ErrorMessageVisible(false);
    }
    self.ShowAlert = function () {
        self.ErrorMessageVisible(true);
    }

    self.LastRequest = null;
    self.Search = function () {
        var searchCriteria = {
            Name: self.NameSearchText(),
            JobTitle: self.JobTitleSearchText(),
            Enabled: self.SelectedSearchState()
        }

        if (self.LastRequest != null && self.SearchInProgress()) {
            self.SearchInProgress(false);
            self.LastRequest.abort();
        }

        self.SearchInProgress(true);
        self.LastRequest = $.ajax({
            url: '/api/Contacts',
            type: 'GET',
            data: jQuery.param(searchCriteria),
            success: function (data) {
                self.CloseAlert();
                self.SearchResults.removeAll();
                $.each(data, function (index, item) {
                    self.SearchResults.push(new Contact({
                        Id: item.Id,
                        Name: item.Name,
                        JobTitle: item.JobTitle,
                        PhoneNumber: item.PhoneNumber,
                        Enabled: item.Enabled
                    }));
                });
            },
            error: function () {
                if (self.SearchInProgress())
                    self.ShowAlert();
            },
            complete: function () {
                self.SearchInProgress(false);
            }
        });
    };

    self.NameSearchText.subscribe(function () { self.Search(); }, this);
    self.JobTitleSearchText.subscribe(function () { self.Search(); }, this);
    self.SelectedSearchState.subscribe(function () { self.Search(); }, this);

    self.ShowSearchView();
    self.Search();
}
///#source 1 1 /Scripts/App/CustomBindings.js
ko.bindingHandlers['fadeVisible'] = {
    'init': function (element, valueAccessor) {
        var value = valueAccessor();
        $(element).toggle(ko.unwrap(value));
    },
    'update': function (element, valueAccessor) {
        var value = valueAccessor();
        ko.unwrap(value) ? $(element).fadeIn() : $(element).fadeOut();
    }
};
///#source 1 1 /Scripts/App/binder.js
LoadTemplatesAndBind = function () {
    $.get("../Views/Templates/bundledTemplates.min.html", function (templates) {
        $("body").append(templates);
        var contactsViewModel = new Contacts();
        ko.applyBindings(contactsViewModel);
    });
};
LoadTemplatesAndBind();

