using ContactManagementService.Models;

namespace ContactManagementService.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ContactManagementServiceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ContactManagementServiceContext context)
        {
            context.Contacts.AddOrUpdate(x => x.Id,
                new Contact { Id = 1, Name = "Hubert Jablonski", JobTitle = ".NET Developer", Enabled = true, PhoneNumber = "+48 880 632 731" },
                new Contact { Id = 2, Name = "John Smith", JobTitle = "UX Designer", Enabled = true, PhoneNumber = "+ 48 123 456 789" },
                new Contact { Id = 3, Name = "Mark Smith", JobTitle = "Senior .NET Developer", Enabled = true, PhoneNumber = "+ 48 987 654 321" },
                new Contact { Id = 4, Name = "Bill Gates", JobTitle = "Chairman", Enabled = true, PhoneNumber = "+ 48 987 654 321" },
                new Contact { Id = 5, Name = "Simon Gates", JobTitle = "Junior .NET Developer", Enabled = true, PhoneNumber = "+ 48 987 654 321" },
                new Contact { Id = 6, Name = "Tom Hanks", JobTitle = "UX Designer", Enabled = true, PhoneNumber = "+ 48 987 654 321" },
                new Contact { Id = 7, Name = "Steffanie Kwak", JobTitle = "Chief Engineer", Enabled = true, PhoneNumber = "+ 48 987 654 321" },
                new Contact { Id = 8, Name = "Alaina Debellis", JobTitle = "Senior .NET Developer", Enabled = true, PhoneNumber = "+ 48 987 654 321" },
                new Contact { Id = 9, Name = "Clotilde Leu", JobTitle = "Contract Engineer", Enabled = true, PhoneNumber = "+ 48 987 654 321" },
                new Contact { Id = 10, Name = "Donella Andrzejewski", JobTitle = "Senior .NET Developer", Enabled = true, PhoneNumber = "+ 48 987 654 321" },
                new Contact { Id = 11, Name = "Sina Steffensen", JobTitle = "Lead Engineer", Enabled = true, PhoneNumber = "+ 48 987 654 321" },
                new Contact { Id = 12, Name = "Jerry Ruffing", JobTitle = "Lead Architect", Enabled = true, PhoneNumber = "+ 48 987 654 321" },
                new Contact { Id = 13, Name = "Thora Koziel", JobTitle = "Principal Software Developer", Enabled = false, PhoneNumber = "+ 48 987 654 321" },
                new Contact { Id = 14, Name = "Chrissy Brayboy", JobTitle = "Maintenance Engineer", Enabled = false, PhoneNumber = "+ 48 987 654 321" },
                new Contact { Id = 15, Name = "Billie Niven", JobTitle = "Manager of Engineering", Enabled = false, PhoneNumber = "+ 48 987 654 321" }
               );
        }
    }
}
